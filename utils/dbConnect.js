require('dotenv').config();

const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/auction-app', {useNewUrlParser: true});
const db = mongoose.connection;
// To log errors on connection 
db.on('error', (error) => {
  console.log(error);
})
// To inform that database connection is successful
db.on('open', () => {
  console.log('Connected to Database successfully!');
})