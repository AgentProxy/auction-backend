# auction-backend


### Project setup
1. Run ```npm install``` to install the dependencies for the frontend vue app
2. Run ```npm run start``` to fire up the app (default port is 3000). Make sure that MongoDb is running.
3. Import sample data from sample folder to populate the database
### Features Implemented
1. Displaying of products in the home page
2. Search products via name and description and retrieve them
3. Getting of product details
4. Getting of product bids
5. Submitting of bid

### Project Description
This is the back-end for the antique auction app. The database used for this app is MongoDB. The stack used for this app is Node, Express, and Mongoose. The cors package was used to enable cors and allow communication with the frontend app.

