const cors = require('cors');
const express = require('express');
const app = express();
// To enable cors
app.use(cors());

app.use(express.json());

const api = require('./routes/router');

app.use('/api', api);

require('./utils/dbConnect')

app.listen(3000, function() {
  console.log('Server started on port 3000...')
})