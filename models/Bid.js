const mongoose = require('mongoose');

const BidSchema = new mongoose.Schema({
  bidAmount: {type: Number, required: true},
  dateBid: {type: Date, required: true, default: Date.now},
  productCode: {type: String, required: true},
  username: {type: String, required: true},
}, { id: false });

module.exports = mongoose.model('Bid', BidSchema);