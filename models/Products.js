const mongoose = require('mongoose');

const ProductsSchema = new mongoose.Schema({
  name: {type: String, required: true},
  productCode: {type: String, required: true, unique: true},
  imageUrl: {type: String},
  description: {type: String},
  price: {type: Number, required: true},
  bidStart: {type: Date, required: true},
  bidEnd: {type: Date, required: true},
  dateAdded: {type: Date, default: Date.now},
}, {id: false});

ProductsSchema.virtual('bids', {
  ref: 'Bid', // The model to use
  localField: 'productCode', // Find people where `localField`
  foreignField: 'productCode', // is equal to `foreignField`
  // If `justOne` is true, 'members' will be a single doc as opposed to
  // an array. `justOne` is false by default.
  justOne: false,
  options: { sort: { dateBid: -1 }} // Query options, see http://bit.ly/mongoose-query-options
});

ProductsSchema.virtual('latestBid', {
  ref: 'Bid', // The model to use
  localField: 'productCode', // Find people where `localField`
  foreignField: 'productCode', // is equal to `foreignField`
  // If `justOne` is true, 'members' will be a single doc as opposed to
  // an array. `justOne` is false by default.
  justOne: true,
  options: { sort: { dateBid: -1 }} // Query options, see http://bit.ly/mongoose-query-options
});


ProductsSchema.virtual('productBids', {
  ref: 'Bid', // The model to use
  localField: 'productCode', // Find people where `localField`
  foreignField: 'productCode', // is equal to `foreignField`
  count: true,
});

ProductsSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Products', ProductsSchema);