
const Products = require('../models/Products');

module.exports = (router) => {
  router.get('/getProductsList', async (req,res) => {
    const searchText = req.query.searchText || ''
    const products = await Products.find({"$or": [ { "name" : { $regex: searchText, $options: "i" }}, {"description": { $regex: searchText, $options: "i" }}] })
      .select('-_id name productCode description price dateAdded imageUrl')
      .skip(req.query.skip || 0)
      .limit(10)
      .populate('productBids')
    res.json(products)
  })
}