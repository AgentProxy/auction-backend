
const allowedAccounts = [
  {
    userId: '1',
    username: 'user1',
    password: 'user1_pass'
  },
  {
    userId: '2',
    username: 'user2',
    password: 'user2_pass'
  }
]


module.exports = (router) => {
  router.post('/login', (req,res) => {
    // Dummy authentication
    if(!req.body || !req.body.username || !req.body.password){
      res.status(403).json({message: 'Incomplete parameters passed.'})
     } else {
       const username = req.body.username;
       const password = req.body.password;
      const targetAccount = allowedAccounts.find((account) => account.username === username && account.password === password)
      // Check if username is retrieved which means that this is a valid user
      if(targetAccount && targetAccount.username) {
        res.send(targetAccount.username);
      } else {
        res.status(400).json({message: 'Please check your username and password.'})
      }
    }
  })
}