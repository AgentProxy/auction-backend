
const Bid = require('../models/Bid');

module.exports = (router) => {
  router.get('/getProductBids/:productCode', async (req,res) => {
    const productBids = await Bid.find({productCode: req.params.productCode})
      .select('-_id username productCode bidAmount dateBid').sort({'dateBid': -1})
    res.json(productBids)
  })
  
}