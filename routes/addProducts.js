const Products = require('../models/Products')

module.exports = (router) => {
  router.post('/addProducts', async (req,res) => {
    try{
      const products = new Products({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        bidStart: new Date(req.body.bidStart),
        bidEnd: new Date(req.body.bidEnd)
      })
      const newProduct = await products.save()
      res.status(201).json(newProduct)
    } catch(err) {
      res.status(400).json({message: err.message})
    }
    
  })
  
}