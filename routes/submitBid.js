const Bid = require('../models/Bid')

const getLatestBid = async (productCode) => {
  // Get the latest bid details for the product
  const latestBid = await Bid.findOne({productCode}).sort({dateBid: -1});
  return latestBid;
}

module.exports = (router) => {
  router.post('/submitBid', async (req,res) => {
    try{
      // Make sure that all parameters are complete
      if(!req.body || !req.body.productCode || !req.body.username || !req.body.bidAmount){
        throw new Error('Incomplete parameters passed.');
      } else {
        const username = req.body.username;
        const bidAmount = req.body.bidAmount;
        const productCode = req.body.productCode;
        const latestBid = await getLatestBid(productCode);
        // Check if current bidder is same as last bidder
        // and throw error to prevent bidder to bid again
        if(latestBid.username === username) {
          throw new Error('You are already the highest bidder!')
        }
        // Make sure that current bid amount is greater than the latest bid amount
        if(latestBid.bidAmount > bidAmount ){
          throw new Error('Please bid a higher amount!');
        }
        const bid = new Bid({
          bidAmount: req.body.bidAmount,
          username: req.body.username,
          productCode: req.body.productCode
        })
        const newBid = await bid.save()
        res.status(201).json(newBid)
      }
    } catch(err) {
      res.status(400).json({message: err.message})
    }
    
  })
  
}