const express = require('express');
const router = express.Router()

require('./addProducts')(router);
require('./login')(router);
require('./getProductsList')(router);
require('./getProductBids')(router);
require('./getProductDetails')(router);
require('./submitBid')(router);

module.exports = router;



