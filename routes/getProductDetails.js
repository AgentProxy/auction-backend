
const Products = require('../models/Products');

module.exports = (router) => {
  router.get('/getProductDetails/:productCode', async (req,res) => {
    const productDetails = await Products.findOne({productCode: req.params.productCode})
      .select('-_id name description price dateAdded productCode imageUrl')
      .skip(req.query.skip || 0)
      .limit(10)
      .populate({
        path: 'latestBid',
        select: '-_id dateBid bidAmount username'
      })
    res.json(productDetails)
  })
  
}